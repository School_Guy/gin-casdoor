module gitlab.com/School_Guy/gin-casdoor

go 1.16

require (
	github.com/casdoor/casdoor-go-sdk v0.0.1
	github.com/gin-gonic/gin v1.7.2
)

package gin_casdoor

import (
	"github.com/casdoor/casdoor-go-sdk/auth"
	"github.com/gin-gonic/gin"
)

type CasdoorAuthReturn struct {
	code  string `form:"code"`
	state string `form:"state"`
}

type CasdoorConfig struct {
	Endpoint         string
	ClientId         string
	ClientSecret     string
	JwtSecret        string
	OrganizationName string
}

func Auth(config CasdoorConfig) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth.InitConfig(config.Endpoint, config.ClientId, config.ClientSecret, config.JwtSecret,
			config.OrganizationName)

		var authReturn CasdoorAuthReturn
		err := ctx.ShouldBind(&authReturn)
		if err != nil {
			ctx.AbortWithError(400, err)
		}

		token, err := auth.GetOAuthToken(authReturn.code, authReturn.code)
		if err != nil {
			ctx.AbortWithError(401, err)
		}

		claims, err := auth.ParseJwtToken(token.AccessToken)
		if err != nil {
			ctx.AbortWithError(401, err)
		}

		claims.AccessToken = token.AccessToken
	}
}
